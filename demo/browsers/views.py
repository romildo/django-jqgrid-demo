from django.http import HttpResponse
from grids import BrowserGrid

def grid_handler(request):
    # handles pagination, sorting and searching
    grid = BrowserGrid()
    return HttpResponse(grid.get_json(request), mimetype="application/json")

def grid_config(request):
    # build a config suitable to pass to jqgrid constructor   
    grid = BrowserGrid()
    return HttpResponse(grid.get_config(), mimetype="application/json")
