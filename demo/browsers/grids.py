from django.core.urlresolvers import reverse_lazy
from jqgrid import JqGrid
from models import Browser

class BrowserGrid(JqGrid):
    model = Browser # could also be a queryset
    url = reverse_lazy('grid_handler')
    caption = 'Grid of browsers' # optional
    colmodel_overrides = {
        'id': { 'editable': False, 'width':10 },
    }
