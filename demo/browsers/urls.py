from django.conf.urls import patterns, url
from views import grid_handler, grid_config

urlpatterns = patterns('',
    url(r'^grid/$', grid_handler, name='grid_handler'),
    url(r'^grid/cfg/$', grid_config, name='grid_config'),
)
