from django.db import models

# Create your models here.

class Browser(models.Model):
    name = models.CharField(max_length=30)
    engine = models.CharField(max_length=20)
    engine_version = models.CharField(max_length=6)
    css_grade = models.CharField(max_length=1)
    platforms = models.CharField(max_length=30)
